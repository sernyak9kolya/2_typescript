import { callApi } from '../helpers/apiHelper';
import { ID, IFighter, IFighterDetails } from '../helpers/mockData';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET') as IFighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: ID): Promise<IFighterDetails> {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET') as IFighterDetails;

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
