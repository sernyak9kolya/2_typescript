interface IHTMLElementProps {
  tagName: string,
  className?: string,
  attributes?: { [key: string]: string }
};

export function createElement({ tagName, className, attributes = {} }: IHTMLElementProps): HTMLElement {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: Array<string> = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key: string): void => element.setAttribute(key, attributes[key]));

  return element;
};
