import { fighters, fightersDetails, ID, IFighter, IFighterDetails } from './mockData';

type ApiData = IFighter[] | IFighterDetails;

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = false;

async function callApi(endpoint: string, method: string): Promise<ApiData> {
  const url: string = API_URL + endpoint;
  const options: { [key: string]: string } = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result): ApiData => JSON.parse(atob(result.content)))
        .catch((error: Error) => {
          throw error;
        });
};

async function fakeCallApi(endpoint: string): Promise<ApiData> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise<ApiData>((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
};

function getFighterById(endpoint: string): IFighterDetails | undefined {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: ID = endpoint.substring(start + 1, end);

  return fightersDetails.find((fighter: IFighterDetails) => fighter._id === id);
};

export { callApi };
