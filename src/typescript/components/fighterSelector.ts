import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { ID, IFighterDetails } from "../helpers/mockData";

export function createFightersSelector() {
  let selectedFighters: IFighterDetails[] = [];

  return async (event: Event, fighterId: ID): Promise<void> => {
    const fighter: IFighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: IFighterDetails = playerOne ?? fighter;
    const secondFighter: IFighterDetails = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
};

const fighterDetailsMap: Map<ID, IFighterDetails> = new Map();

export async function getFighterInfo(fighterId: ID): Promise<IFighterDetails> {
    if (!fighterDetailsMap.has(fighterId)) {
        const fighter: IFighterDetails = await fighterService.getFighterDetails(fighterId);
        fighterDetailsMap.set(fighterId, fighter);
    }

    return fighterDetailsMap.get(fighterId)!;
};

function renderSelectedFighters(selectedFighters: IFighterDetails[]): void {
  const fightersPreview = document.querySelector('.preview-container___root') as HTMLDivElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLDivElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLDivElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLDivElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]): HTMLDivElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = (): void => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' }) as HTMLDivElement;
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  }) as HTMLImageElement;
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  }) as HTMLButtonElement;

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]): void {
  renderArena(selectedFighters);
}
