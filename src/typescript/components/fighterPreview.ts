import { createElement } from '../helpers/domHelper';
import { IFighterDetails } from "../helpers/mockData";

export function createFighterPreview(fighter: IFighterDetails, position: string) {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  }) as HTMLDivElement;
  const fighterName: HTMLHeadingElement = createFighterName(fighter);
  const fighterImage: HTMLImageElement = createFighterImage(fighter);
  const fighterStats: HTMLUListElement = createFighterStats(fighter);

  fighterElement.append(fighterName);
  fighterElement.append(fighterImage);
  fighterElement.append(fighterStats);

  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails) {
  const { source, name } = fighter;
  const attributes: { [key: string]: string } = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  }) as HTMLImageElement;

  return imgElement;
}

function createFighterName(fighter: IFighterDetails) {
  const { name } = fighter;
  const nameElement = createElement({
    tagName: 'h4',
    className: 'fighter-preview___name',
  }) as HTMLHeadingElement;

  nameElement.innerText = name;

  return nameElement;
}

function createFighterStats(fighter: IFighterDetails) {
  const { attack, defense, health } = fighter;
  const statsElement = createElement({
    tagName: 'ul',
    className: 'fighter-preview___stats',
  }) as HTMLUListElement;

  const healthElement: HTMLLIElement = createFighterStatsItem({
    name: 'Health',
    value: health,
  });
  const attackElement: HTMLLIElement = createFighterStatsItem({
    name: 'Attack',
    value: attack,
  });
  const defenseElement: HTMLLIElement = createFighterStatsItem({
    name: 'Defense',
    value: defense,
  });

  statsElement.append(healthElement);
  statsElement.append(attackElement);
  statsElement.append(defenseElement);

  return statsElement;
}

function createFighterStatsItem(stat: { name: string, value: number }) {
  const { name, value } = stat;
  const itemElement = createElement({
    tagName: 'li',
    className: 'fighter-preview___stats-item',
  }) as HTMLLIElement;

  itemElement.innerHTML = `${name}: ${value}`;

  return itemElement;
}