import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighter, IFighterDetails } from '../helpers/mockData';

type PairOfFighters = [IFighterDetails, IFighterDetails];

export async function renderArena(selectedFighters: IFighterDetails[]): Promise<void> {
  const root = document.getElementById('root') as HTMLDivElement;
  const arena: HTMLDivElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  const winner: IFighterDetails = await fight(...selectedFighters as PairOfFighters);
  showWinnerModal(winner as IFighter);
};

function createArena(selectedFighters: IFighterDetails[]): HTMLDivElement {
  const arena = createElement({ tagName: 'div', className: 'arena___root' }) as HTMLDivElement;
  const healthIndicators: HTMLDivElement = createHealthIndicators(...selectedFighters as PairOfFighters);
  const fighters = createFighters(...selectedFighters as PairOfFighters);

  arena.append(healthIndicators, fighters);
  return arena;
};

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails): HTMLDivElement {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' }) as HTMLDivElement;
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' }) as HTMLDivElement;
  const leftFighterIndicator: HTMLDivElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator: HTMLDivElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
};

function createHealthIndicator(fighter: IFighterDetails, position: string): HTMLDivElement {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' }) as HTMLDivElement;
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' }) as HTMLSpanElement;
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' }) as HTMLDivElement;
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }}) as HTMLDivElement;

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
};

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails): HTMLDivElement {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` }) as HTMLDivElement;
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
};

function createFighter(fighter: IFighterDetails, position: string): HTMLDivElement {
  const imgElement: HTMLImageElement = createFighterImage(fighter);
  const positionClassName: string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  }) as HTMLDivElement;

  fighterElement.append(imgElement);
  return fighterElement;
};
