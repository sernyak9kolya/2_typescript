import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { IFighter } from '../../helpers/mockData';

export function showWinnerModal(fighter: IFighter): void {
  const { name, source } = fighter;
  const bodyElement = createElement({
      tagName: 'div',
      className: 'modal-body',
  }) as HTMLDivElement;
  const textElement = createElement({
      tagName: 'p',
      className: 'modal-text',
  }) as HTMLParagraphElement;
  const imgAttributes: { [key: string]: string } = {
    src: source,
    alt: name,
    title: name,
  }
  const imgElement = createElement({
      tagName: 'img',
      className: 'modal-image',
      attributes: imgAttributes,
  }) as HTMLImageElement;

  textElement.innerText = `${name} won`;

  bodyElement.append(textElement);
  bodyElement.append(imgElement);

  showModal({
      title: "Game over!",
      bodyElement
  });
};
