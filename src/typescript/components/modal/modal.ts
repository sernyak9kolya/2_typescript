import { createElement } from '../../helpers/domHelper';

interface IModalProps {
  title: string,
  bodyElement: HTMLElement,
  onClose?: () => void
};

export function showModal({ title, bodyElement, onClose = () => {} }: IModalProps) {
  const root: HTMLDivElement = getModalContainer();
  const modal: HTMLDivElement = createModal({ title, bodyElement, onClose });

  root.append(modal);
};

function getModalContainer(): HTMLDivElement {
  return document.getElementById('root') as HTMLDivElement;
};

function createModal({ title, bodyElement, onClose }: IModalProps): HTMLDivElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' }) as HTMLDivElement;
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' }) as HTMLDivElement;
  const header: HTMLDivElement = createHeader(title, onClose!);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
};

function createHeader(title: string, onClose: () => void): HTMLDivElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' }) as HTMLDivElement;
  const titleElement = createElement({ tagName: 'span' }) as HTMLSpanElement;
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' }) as HTMLDivElement;

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = (): void => {
    hideModal();
    onClose();
  };

  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName('modal-layer')[0] as HTMLDivElement;
  modal?.remove();
};
