import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { ID, IFighter } from "../helpers/mockData";

export function createFighters(fighters: IFighter[]): HTMLDivElement {
  const selectFighter: (event: Event, fighterId: ID) => void = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root'}) as HTMLDivElement;
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' }) as HTMLDivElement;
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' }) as HTMLDivElement;
  const fighterElements: HTMLDivElement[] = fighters.map(
    (fighter: IFighter): HTMLDivElement => createFighter(fighter, selectFighter)
  );

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
};

function createFighter(fighter: IFighter, selectFighter: (event: Event, fighterId: ID) => void): HTMLDivElement {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' }) as HTMLDivElement;
  const imageElement: HTMLImageElement = createImage(fighter);
  const onClick = (event: MouseEvent): void => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
};

function createImage(fighter: IFighter): HTMLImageElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  }) as HTMLImageElement;

  return imgElement;
};
