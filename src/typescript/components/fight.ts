import { controls } from '../../constants/controls';
import { IFighterDetails } from '../helpers/mockData';

const TEN_SECONDS_IN_MS: number = 10000;

interface IPlayer extends IFighterDetails {
  isBlocking: boolean,
  currentHealth: number,
  canUseCombo: boolean,
  comboTimer: NodeJS.Timeout | null,
  comboKeysPressed: Set<string>,
  healthIdicatorElement: HTMLDivElement
};

export function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<IFighterDetails> {
  const playerOne: IPlayer = createPlayer(firstFighter, 'left');
  const playerTwo: IPlayer = createPlayer(secondFighter, 'right');

  const isGameOver = (): boolean => playerOne.currentHealth <= 0 || playerTwo.currentHealth <= 0;

  const handleKeyDown = (event: KeyboardEvent) => {
    const {
      PlayerOneBlock,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;

    switch(event.code) {
      case PlayerOneBlock: return setBlock(playerOne);
      case PlayerTwoBlock: return setBlock(playerTwo);
    };

    const playerOneComboKey: string | undefined = PlayerOneCriticalHitCombination.find(k => k === event.code);
    if (playerOneComboKey) {
      setComboKeyPressed(playerOne, playerOneComboKey);
      useCombo(playerOne, playerTwo, PlayerOneCriticalHitCombination);
    }

    const playerTwoComboKey: string | undefined = PlayerTwoCriticalHitCombination.find(k => k === event.code);
    if (playerTwoComboKey) {
      setComboKeyPressed(playerTwo, playerTwoComboKey);
      useCombo(playerTwo, playerOne, PlayerTwoCriticalHitCombination);
    }
  }

  const handleKeyUp = (event: KeyboardEvent) => {
    const {
      PlayerOneAttack, PlayerOneBlock,
      PlayerTwoAttack, PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;

    switch(event.code) {
      case PlayerOneBlock: return unsetBlock(playerOne);
      case PlayerTwoBlock: return unsetBlock(playerTwo);
      case PlayerOneAttack: return attack(playerOne, playerTwo);
      case PlayerTwoAttack: return attack(playerTwo, playerOne);
    }

    const playerOneComboKey: string | undefined = PlayerOneCriticalHitCombination.find(k => k === event.code);
    if (playerOneComboKey) {
      return unsetComboKeyPressed(playerOne, playerOneComboKey);
    }

    const playerTwoComboKey: string | undefined = PlayerTwoCriticalHitCombination.find(k => k === event.code);
    if (playerTwoComboKey) {
      return unsetComboKeyPressed(playerTwo, playerTwoComboKey);
    }
  };


  return new Promise<IFighterDetails>((resolve) => {

    const gameOver = (): void => {
      const winner: IFighterDetails = playerOne.currentHealth <= 0 ? secondFighter : firstFighter;

      window.removeEventListener('keyup', onKeyUp);
      window.removeEventListener('keydown', onKeyDown);

      clearTimeout(playerOne.comboTimer!);
      clearTimeout(playerTwo.comboTimer!);

      resolve(winner);
    };

    const onKeyUp = (event: KeyboardEvent): void => {
      handleKeyUp(event);

      if (isGameOver()) {
        gameOver();
      }
    };

    const onKeyDown = (event: KeyboardEvent): void => {
      handleKeyDown(event);

      if (isGameOver()) {
        gameOver();
      }
    };

    window.addEventListener('keyup', onKeyUp);
    window.addEventListener('keydown', onKeyDown);
  });
};

export function getDamage(attacker: IPlayer, defender: IPlayer): number {
  if (defender.isBlocking) return 0;

  const damage: number = getHitPower(attacker) - getBlockPower(defender);

  return Math.max(0, damage);
};

export function getCriticalDamage(attacker: IPlayer): number {
  const { attack } = attacker;
  const damage: number = attack * 2;

  return damage;
};

export function getHitPower(attacker: IPlayer): number {
  const { attack } = attacker;
  const criticalHitChance: number = Math.random() + 1;
  const power: number = attack * criticalHitChance;

  return power;
};

export function getBlockPower(defender: IPlayer): number {
  const { defense } = defender;
  const dodgeChance: number = Math.random() + 1;
  const power: number = defense * dodgeChance;

  return power;
};

function createPlayer(fighter: IFighterDetails, side: string): IPlayer {
  return {
    isBlocking: false,
    canUseCombo: true,
    comboKeysPressed: new Set(),
    comboTimer: null,
    currentHealth: fighter.health,
    healthIdicatorElement: document.getElementById(`${side}-fighter-indicator`) as HTMLDivElement,
    ...fighter
  };
};

function setHealthIndicator(player: IPlayer): void {
  const { health, currentHealth, healthIdicatorElement } = player;

  let currentHealthInPercent: number = currentHealth / health * 100;
  currentHealthInPercent = Math.max(0, currentHealthInPercent);

  healthIdicatorElement.style.width = `${currentHealthInPercent}%`;
};

function attack(attacker: IPlayer, defender: IPlayer): void {
  if (!attacker.isBlocking && !defender.isBlocking) {
    const damage: number = getDamage(attacker, defender);
    return setDamage(defender, damage);
  }
};

function setDamage(defender: IPlayer, damage: number): void {
  defender.currentHealth -= damage;
  setHealthIndicator(defender);
};

function setBlock(player: IPlayer): void {
  player.isBlocking = true;
};

function unsetBlock(player: IPlayer): void {
  player.isBlocking = false;
};

function setComboKeyPressed(player: IPlayer, key: string): void {
  player.comboKeysPressed.add(key);
};

function unsetComboKeyPressed(player: IPlayer, key: string): void {
  player.comboKeysPressed.delete(key);
};

function setUsingCombo(player: IPlayer): void {
  player.canUseCombo = true;
  player.healthIdicatorElement.style.backgroundColor = '#259bef';
};

function unsetUsingCombo(player: IPlayer): void {
  player.canUseCombo = false;
  player.healthIdicatorElement.style.backgroundColor = '#ebd759';
};

function useCombo(attacker: IPlayer, defender: IPlayer, comboKeys: string[]): void {
  const attackerUseCombo: boolean = attacker.comboKeysPressed.size === comboKeys.length;
  if (attackerUseCombo && attacker.canUseCombo) {
    const criticalDamage: number = getCriticalDamage(attacker);
    usedCombo(attacker);
    setDamage(defender, criticalDamage);
  }
};

function usedCombo(attacker: IPlayer): void {
  unsetUsingCombo(attacker);
  attacker.comboTimer = setTimeout((): void => setUsingCombo(attacker), TEN_SECONDS_IN_MS);
};
